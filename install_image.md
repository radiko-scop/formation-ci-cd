### Sur la VM

- ssh ubuntu@141.95.165.171

```
sudo apt install docker.io
sudo apt install docker-compose
export GITLAB_HOME=/srv/gitlab
```

### Sur l'hôte

- Replace 141.95.165.171 with VM public address in docker-compose.yml 
- `scp Caddyfile  ubuntu@141.95.165.171:/home/ubuntu`
- `scp index.html  ubuntu@141.95.165.171:/home/ubuntu`
- `scp docker-compose.yml ubuntu@141.95.165.171:/home/ubuntu`

### Sur la VM

- `sudo docker volume create --name=caddy_data`
- `sudo docker-compose up -d`

- To connect as *root* to your gitlab instance
  - `sudo cat /srv/gitlab/config/initial_root_password` [2024-04: does not work]
  - or `sudo docker exec -it <container id> bash` & `cat /etc/gitlab/initial_root_password`
  - or follow tuto using rails to reset user password

root Password: VIvuPLZXC0O/+AfXQCL2SAJ/98OAq7ovc5DUH4B1pi0=


- Once connected go to Settings/General and modify Signup restrictions [2024-04: not needed anymore].

Once account created:
[bf@laptop-ben test_ci]$ git clone http://[2001:41d0:404:100::122e]/benjaminforest/test_ci 

- Add runner:
  * `docker exec -it gitlab-runner /bin/bash`
  * `gitlab-runner register` then answer questions (image type is docker)


- Add SonarQube
http://formation-python.radiko.fr:9000

### Configurer son git en local:

[2024-04] je ne sais pas à quoi sert cette info. SI on utilise le mot de passe pas de problème.

Note port 2222, deux comptes ne peuvent pas avoir la meme clef ssh.
git config --local core.sshCommand "/usr/bin/ssh -i /home/bf/dev/radiko/formation/2023-08-tmp-tp/random_project/lab_ci -p 2222"

### Faire marche Caddy

scp Caddyfile  ubuntu@51.91.148.161:/home/ubuntu
scp index.html  ubuntu@51.91.148.161:/home/ubuntu

