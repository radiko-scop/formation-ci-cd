Mail
-------

Bonjour à tous,

J'ai le plaisir d'animer pour vous le TP concernant l'intégration continue. Voici donc une première prise de contact.

On va étudier ensemble les bases de l'intégration continue avec Gitlab, sauf demande différente de votre part.

Prérequis:

- un environnement git fonctionnel
- un environnement python fonctionnel, dans lequel on peut exécuter la commande pip
- quelques bases sur les tests unitaires

Prérequis en terme de connaissances:

- Bases de git. Si vous n'en avez pas, merci de suivre au moins un tutoriel basique.

N'hésitez pas à me signaler, par retour de mail, toute question, commentaire ou remarque

Bonne journée,


Intégration à un gestionnaire de repo git
++++++++++++++++++++++++++++++++++++++++++

Exemple : Gitlab/Github/Bitbucket...

Quelque pensées qui n'engagent que moi sur les différences
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Jenkins est un précurseur, donc garde une interface et une philosophie assez manuelle au niveau de l'installation etc. Les approches plus modernes lancent des containers et déroulent les pipelines dedans, via des fichiers de configuration genre docker.


A propos
-----------

Partons du workflow de développement disons "standard" de git: <nvie ou https://docs.github.com/en/get-started/quickstart/github-flow>. Avant de livrer une version du logiciel, on doit faire des tests notamment. Éventuellement, un contrôle qualité sur le code écrit, si l'on doit respecter certaines normes (ou des standards internes de qualité). Enfin, on devra packager notre logiciel, soit dans une archive, soit avec un installer, soit avec un package, soit dans un container. Peut être faudra-t-il aussi le déployer sur un certain nombre de serveurs.

Selon les entreprises, et les logiciels, on choisit de faire ces opérations de temps en temps et de faire des mises à jour incrémentales lourdes, ou bien régulièrement et de faire de la mise à jour continue.

Dans les deux cas, la tendance est à tenter d'automatiser un maximum les tâches liées à ces activités, qui s'y prêtent assez bien et ne sont pas forcément intéressantes manuellement.

On appelle donc des automates les logiciels chargés de faire ces tâches à notre place.

Problématiques
-----------------

Rapidement, la gestion de l'intégration continue devient un métier car:

- La partie déploiement d'infra est un métier
- Sur de gros projets la partie build et tests peut être très longue et nécessite des optimisations complexe pour rester sous un seuil "acceptable"


Biblio
----------

- RedHat opinion:  https://www.redhat.com/en/topics/devops/what-is-ci-cd
- RedHat opinion : https://www.redhat.com/fr/topics/devops/what-is-ci-cd
- TeamCity https://www.jetbrains.com/teamcity/
- https://gitlab.gnome.org/GNOME/gtk/-/pipelines

- https://github.com/dlemstra/Magick.NET/actions
- https://github.com/dlemstra/Magick.NET/actions/runs/2949835618/workflow
- https://docs.drone.io/runner/docker/overview/


parler des tests unitaires
parler des vulnérabilités (https://checkmarx.com/)

CI : construire binaire + tests + déploiement basique
CD : workflow validation + déploiement binaires, peut être complexe

éviter pyscaffold ?
regarder android / linux CI
firefox

machine propre
cross compilation

juste un serveur qui joue des commandes
events github / gitlab

pipelines pas dans l'usine mais dans git.


Solution :

.. code-block:: yaml

    successful_job:
        script: "(exit 0)"
    failing_job:
        script: "(exit 20)"

mémo:

podman run -d --name gitlab-runner --restart always   -v /home/bf/dev/radiko/formation/formation-ci-cd/tmp/config:/etc/gitlab-runner:Z  -v /var/run/docker.sock:/var/run/docker.sock:Z gitlab/gitlab-runner:latest

podman exec -it gitlab-runner gitlab-runner register

Un petit cours sur comment ça fonctionne les runners ?

https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27119
