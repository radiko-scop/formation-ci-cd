.. include:: <isotech.txt>
.. highlight:: python

.. Pour la présentation, https://obsproject.com/wiki/install-instructions#linux peut servir.

=======================================================================
Continuous Integration - Continuous Delivery - Continuous Deployement
=======================================================================

Cycle de vie logiciel continu

.. TOC
.. =======

.. .. contents::

Introduction
========================

Présentation
-------------

- Benjamin Forest
- Radiko
- benjamin.forest@radiko.fr
- Ces slides sont disponibles sur gitlab : https://gitlab.com/radiko-scop/formation-ci-cd

.. Présentation de moi et radiko. Expériences autour du test.
   Premiere formation pour moi. Soyez acteurs.
   Faire l'appel

Au programme
-------------

.. image:: _static/v_cycle.svg
   :align: center

.. NB : ca reste valable quelque soit la méthode de dev.
   Plan de test
   Tests unitaires et liés
   Test de charge (bonus)
   16h c'est court. Je ne vais pas vous emm*** avec des définitions à gogo.
   Pour les gens qui voudraient plus d'infos, voir la biblio en fin de cours.

Livrable & Notation
--------------------

- Notes individuelles.
- Livrable : 1 zip par **personne**:
    * Compte rendu des TPs (simple). *Au format PDF*.
    * Code source produit
- à envoyer à benjamin.forest@radiko.fr
- Avant le 29 avril 2024

..  Je ne vais pas corriger, juste regarder et mettre une note -> être concis.

À propos des LLMs
--------------------

Il est probable que ChatGPT et compagnie effectuent ce TP mieux que vous.
Leur utilisation doit donc être intelligente.

- Écologie
- Comprehension
- Limites


Pré-requis
========================

Workflows
------------------------

Comment se font les livraisons dans votre entreprise ?

Faites vous des tests ?

Comment gérez vous les bugs ?

Comment vérifiez vous que le logiciel que vous avez corrigé fonctionne encore ?

Comment crééz vous une nouvelle version ?

Avez vous d'autres exemples ?

.. Haute fiabilité
  Normes (Médicales type EN-62304, Aéronautique DO-*)
  Non régression
  Refactoring de code en boite noire

Quelques rappels sur les workflows git
--------------------------------------

.. image:: _static/git-workflow.png
    :align: center


..   Partons du workflow de développement disons "standard" de git: <nvie ou https://docs.github.com/en/get-started/quickstart/github-flow>. Avant de livrer une version du logiciel, on doit faire des tests notamment. Eventuellement, un contrôle qualité sur le code écrit, si l'on doit respecter certaines normes (ou des standards internes de qualité). Enfin, on devra packager notre logiciel, soit dans une archive, soit avec un installer, soit avec un package, soit dans un container. Peut être faudra-t-il aussi le déployer sur un certain nombre de serveurs.

   Selon les entreprises, et les logiciels, on choisit de faire ces opérations de temps en temps et de faire des mises à jour incrémentales lourdes, ou bien régulièrement et de faire de la mise à jour continue.

   Dans les deux cas, la tendance est à tenter d'automatiser un maximum les tâches liées à ces activités, qui s'y prêtent assez bien et ne sont pas forcément intéressantes manuellement.

   On appelle donc des automates les logiciels chargés de faire ces tâches à notre place.


Quelques rappels sur les tests
--------------------------------

+---------------------------+--------------+
| Phase                     | Coût relatif |
+===========================+==============+
| Le design                 | 1            |
+---------------------------+--------------+
| L'implémentation          | 6.5          |
+---------------------------+--------------+
| Les phases de test        | 15           |
+---------------------------+--------------+
| Les phases de maintenance | 100          |
+---------------------------+--------------+

.. On notera donc qu'il faut faire les tests tôt !


Continuous x3
=========================

.. image:: _static/gitlab_workflow_example_11_9.png
    :align: center

source : gitlab


Continuous x3
=========================

.. image:: _static/gitlab_workflow_example_extended_v12_3.png
    :align: center

source : gitlab


Continuous Intégration
=========================


Continuous Delivery
=========================


Continuous Deployment
=========================


A retenir
=========================

Ces trois termes semblent compliqués mais ils désignent simplement *Un script qui irait lire un dépot git et faire qqchose dessus*.


Les solutions Techniques
============================


Jenkins
---------

Gitlab-CI
-----------

Github actions
----------------

Les autres
-----------

Drone - Teamcity - Circle Ci - Travis - Cirrus

https://github.com/ligurio/awesome-ci

Exemple
----------

https://firefox-ci-tc.services.mozilla.com/


Atelier
=============

Faire un projet simple qui intégré de manière continue.

.. note : je vais créer vos comptes gitlab pendant que vous regardez le TP

https://azure.microsoft.com/en-us/products/devops/#overview

.. ::

   Ce cours permet de comprendre les atouts de l'intégration continue et d'apprendre à mettre en place une plateforme Jenkins.
   2.1 – Mise en place du serveur Jenkins
   Jenkins : Job – workspace – tendance
   Les différents types d’installation
   Contenu du répertoire Jenkins
   Configuration des outils (Java, Maven, SCM, Serveur de mail)
   Les Plugins Jenkins
   Automatisation des tâches avec CLI ou l’API Rest
   TP1 : Installation de Jenkins – Configuration Java, Maven - Mise en place de build Maven
   2.2 – Tests et métriques
   Automatisation des tests unitaires et d’intégration
   Mise en place de jobs chainés
   Configuration des rapports
   Intégration de la qualité dans le processus build
   2.3 – Les Outils d’analyses disponibles :
   Checkstyle
   Findbugs
   CPD/PMD
   Sonar
   Configuration du rapport qualité avec le plugin Violations
   TP2 : Installation de différents plugins – exécution de cibles Maven orientées tests – Publication des résultats dans Jenkins – reporting des
   résultats
   TP3 : Mise en place avec Jenkins SVN et Make – Reporting des résultats de couverture et test – Intégration avec Testlink
