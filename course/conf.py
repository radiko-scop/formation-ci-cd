# -*- coding: utf-8 -*-

# -- Project information -----------------------------------------------------
project = "Formation test python"
copyright = "2021, Stéphane Clérambault"
author = "Stéphane Clérambault"
version = ""
release = "2021-03-23"

# -- General configuration ---------------------------------------------------
extensions = ["sphinx_revealjs"]  # , "sphinxcontrib.mermaid"]


def setup(app):
    from sphinxcontrib.mermaid import html_visit_mermaid, mermaid

    app.add_node(mermaid, override=True, revealjs=(html_visit_mermaid, None))


templates_path = ["_templates"]
source_suffix = ".rst"
master_doc = "index"
language = None
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]
pygments_style = None

# -- Options for HTML output -------------------------------------------------
html_theme = "alabaster"
html_static_path = ["_static"]
html_scaled_image_link = False
html_css_files = ["html.css"]

# -- Options for Reveal.js output ---------------------------------------------
highlight_language = "bash"
revealjs_static_path = ["_static"]
revealjs_style_theme = "radiko.css"
revealjs_script_conf = """
    {
        controls: true,
        progress: true,
        history: false,
        center: true,
        transition: "slide",
        slideNumber: "c",
    }
"""
revealjs_script_plugins = [
    {
        "name": "RevealNotes",
        "src": "revealjs4/plugin/notes/notes.js",
    },
    {
        "name": "RevealHighlight",
        "src": "revealjs4/plugin/highlight/highlight.js",
    },
]
revealjs_css_files = [
    "nokia_font.css",
    "revealjs4/plugin/highlight/zenburn.css",
]

mermaid_init_js = "<script>mermaid.initialize({startOnLoad:false});</script>"
# Use a local version of mermaid
mermaid_version = ""
html_js_files = [
    "mermaid.min.js",
    "mermaid_render_on_display.js",
    "logo_on_dark_background.js",
]

# -- Options for HTMLHelp output ---------------------------------------------
htmlhelp_basename = "sphinx-revealjsdoc"

# -- Options for LaTeX output ------------------------------------------------
latex_elements = {}
latex_documents = [
    (
        master_doc,
        "sphinx-revealjs.tex",
        "sphinx-revealjs Documentation",
        "manual",
    ),
]

# -- Options for manual page output ------------------------------------------
man_pages = [
    (master_doc, "sphinx-revealjs", "sphinx-revealjs Documentation", [author], 1)
]

# -- Options for Texinfo output ----------------------------------------------
texinfo_documents = [
    (
        master_doc,
        "sphinx-revealjs",
        "sphinx-revealjs Documentation",
        author,
        "sphinx-revealjs",
        "One line description of project.",
        "Miscellaneous",
    ),
]

# -- Options for Epub output -------------------------------------------------
epub_title = project
epub_exclude_files = ["search.html"]
