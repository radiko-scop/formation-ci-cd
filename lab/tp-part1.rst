.. sectnum::

TP1 : Intégration continue partie 1
======================================

Modalités
-----------

- Un CR par personne. Format libre (papier, .txt, .doc ...) tant que c'est lisible. Des captures d'écrans sont attendues.
- Possibilité de choisir un langage qui vous sert au travail ou dans un projet perso (Attention léger supplément de travail à prévoir)

Objectifs
-----------

Mettre en place un serveur d'intégration continue simple, pour se familiariser avec les concepts de base.

.. class::info

    Il existe de très nombreux outils d'automatisation de pipelines. On peut citer :

    - Teamcity (JetBrains)
    - Jenkins
    - Circle CI
    - `Travis CI <https://www.travis-ci.com/>`_
    - Gitlab CI
    - Github actions
    - Atlassian Bamboo
    - `Cirrus CI <https://cirrus-ci.org/>`_
    - Drone
    - `...<https://github.com/ligurio/awesome-ci>`_

Nous allons nous concentrer sur Gitlab CI, mais la plupart des outils suivent des concepts similaires.

Première étape : avoir un projet versionné avec Git
----------------------------------------------------

Créez un compte sur http://formation-ci.radiko.fr. en suivant les instructions.

.. class::info

    - L'instance gitlab http://formation-ci.radiko.fr est une instance temporaire, qui sera supprimée à la fin du TP. Ne stockez donc rien dessus.


Dans la suite, suivez soit l'option 1 soit l'option 2.

Option 1 (conseillée) : on part d'un projet existant
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- Créez un projet avec votre compte sur http://formation-ci.radiko.fr
- Allez sur http://formation-ci.radiko.fr/xefi/discover-ci (connectez vous à votre compte)
- Forkez le projet

.. image:: fork.png

- Vérifiez que votre espace gitlab contient votre copie du projet
- Clonez votre copie du projet en local
- Ouvrez un terminal
- Crééz un environnement virtuel si vous ne voulez pas l'installer au niveau global (``python -m venv .venv`` puis ``source .venv/bin/activate``)
- Installez tox (``pip install tox``)
- Regarder ce qu'on peut faire avec : ``tox -av``. On va particulièrement s'intéresser à:
    - ``tox -e build`` pour builder votre package python
    - ``tox -e docs`` pour générer la documentation
    - ``tox`` pour faire tourner les tests & générer le coverage
- Regardez également ce qu'on peut faire avec ``python setup.py --help-commands``. Il s'agit de commandes python.
- More on tox https://www.seanh.cc/2018/09/01/tox-tutorial/.
- flake8 peut être intéressant aussi. (en fait, on utilise généralement un pre-commit hook si on veut faire ça,  on en reparlera si on a le temps).

.. class::info

  À stade, nous n'avons rien fait en lien avec la CI/CD. Il s'agit juste d'un projet python basique, prêt à être utilisé.

Option 2 : utilisez votre propre projet
+++++++++++++++++++++++++++++++++++++++++++++++

A discuter avec moi en fonction de ce que vous voulez proposer.

Créer un premier Job
-----------------------------------

Gitlab
+++++++

- Créer un fichier ``.gitlab-ci.yml`` à la racine du projet ou en utilisant l'interface Gitlab.
- En utilisant la documentation (`ici <https://docs.gitlab.com/ee/ci/jobs/index.html>`_), crééz une pipeline avec deux jobs, dont l'un échoue systématiquement. Pensez éventuellement à vos bases de shell, ou bien demandez à google comment faire un script retournant quelque chose.
- Poussez vos modifications sur git (``git add .gitlab-ci.yml``, ``git commit -m "add ci"``, ``git push``)
- Allez consulter la page concernant les pipelines

.. image:: pipeline_screen.png

**Mettre dans le compte-rendu un screenshot de ce que vous obtenez.**

Est-ce conforme à ce que vous attendiez ?

.. class::info

    Si vous avez bien configuré votre compte Gitlab, vous allez peut-être recevoir un mail vous indiquant que la pipeline échoue.

.. class::info

    Si vous utilisez github, le fichier s'appellera .github/workflows/nom_du_workflow.yml

Github
+++++++++

- Créer un fichier .github/workflows/first_pipeline.yml
- A partir de la `doc <https://docs.github.com/en/actions/using-workflows/about-workflows>`_, faire un Job.

{{CORR

Correction
++++++++++++++

Bien conforme : si le script exécuté échoue, la pipeline échoue


Exemple de yml:

.. code-block:: yaml

    job1_that_succeed:
      script: echo "coucou"
     
    job2_that_fails:
      script: "false"
  
CORR}}


Faire tourner les tests unitaires
-----------------------------------------------

En vous aidant de ce qu'on a vu plus haut sur tox, ou de ce qu'on a fait lors du module précédent, modifiez votre pipeline pour qu'elle exécute les tests unitaires.
Votre premier essai va certainement échouer.

**Mettez un screenshot de ce qui échoue dans votre compte rendu.**

Commentez le message d'erreur. Vous parait-il pertinent ?

Pour corriger l'erreur,  il faut comprendre que les Jobs gitlab tournent dans des container: https://docs.gitlab.com/ee/ci/docker/using_docker_images.html.

Par défaut, le container ne contient que ruby. Pour avoir un container qui contient python, il faut ajouter ``image: "python:3.10"`` au fichier de configuration de la CI. Gitlab ira alors chercher un container officiel du même nom, tout bien configuré pour que python tourne.

Pensez vous que cela suffira pour que vos tests s'exécutent correctement ? Si non, pourquoi ?

L'image dont on dispose n'a malheureusement pas tox/pytest installé par défaut. Il faut donc ajouter les lignes suivantes:

.. code-block:: yaml

  before_script:
    - pip install -U tox #(ou pytest)

à votre fichier de configuration (``.gitlab-ci.yml``) pour que le job fasse passer les tests.

.. class::info

    Gitlab dispose d'un éditeur de pipeline intégré, qui vous évitera de faire des allers retours avec git.
    .. image:: pipeline_editor.png
    
.. class::info

    L'option -U indique qu'on veut upgrader tox si la dernière version n'est pas installée.
    
.. class::info

    Si vous utilisez github, il faudra simplement ajouter un stepx²
    
Prenez une capture d'écran du terminal avec les tests qui passent.

Ajoutez un test qui échoue pour vérifier que le résultat des tests est bien pris en compte.

**Screenshot du test échouant sur Gitlab dans le compte-rendu.**

Faire tourner un outil supplémentaire pendant les tests
---------------------------------------------------------

On se propose, en plus des tests unitaires, de faire tourner un outil vérifiant la qualité du code


Essayez en local : ``flake8 src/`` (installez le avec ``pip install flake8`` si nécessaire).
Est il satisfait ?


Ajoutez un import au début de l'un des fichiers de src (ex ``import socket``). Relancer flake8. Que dit-il ?
A quoi sert ce genre d'outils ?


On se propose de faire tourner également flake8 durant la phase de test. Ajoutez un nouveau Job effectuant ce test.


Prendre un screenshot avec flake8 échouant sur Gitlab.

Organiser ses jobs en stages
-------------------------------------------

Nous avons maintenant la capacité de faire tourner un nombre important de tâches, à chaque fois qu'un commit est fait.
Il peut être intéressant de grouper ces jobs, car ils tournent par défaut tous en parallèle. Gitlab fourni pour cela la notion de `stage <https://docs.gitlab.com/ee/ci/yaml/index.html#stages.>`_.

Suivez l'exemple donné dans la documentation pour créer les trois stages ``test, release, doc``.
Ajoutez les tests unitaire au stage "test"

Ajoutez la génération de la documentation à l'étape doc.

Proposez une méthode pour ne pas redéfinir l'image et l'installation de tox à chaque étape (indice : la doc de ``before_script`` propose des choses).

Insérez un screenshot du résultat dans votre compte rendu.

{{CORR

Correction
++++++++++++++

.. code-block:: yaml

    stages:
      - test
      - release
      - doc

    default:
      before_script:
        - pip install -U tox flake8 #(ou pytest)

    unit_tests:
      stage: test
      script: 
        - tox
        - flake8

    generate_doc:
      stage: doc
      script: tox -e docs

CORR}}

Limiter le nombre de fois ou la pipeline est exécutée
----------------------------------------------------------------------

N'oublions pas que le fait de faire tourner une pipeline a un coût (écologique + euros) -> il est pertinent de ne pas la faire tourner sur un commit pour lequel on sait qu'elle va échouer.

Proposez une pipeline dans laquelle:

- Les tests unitaires sont effectués systématiquement.
- La documentation n'est créée que lorsqu'on crée une merge request dans master, ou que l'on clique sur le bouton le demandant.

On pourra pour cela utiliser la notion de `job control <https://docs.gitlab.com/ee/ci/jobs/job_control.html>`_

Screenshot montrant que ça fonctionne.

.. class::info

    Pour voir les effets de la règle appliquée, il faudra sans doutes créer une branche de feature, sinon pas de merge request possible !

.. class::info

    Profitez en, si vous ne l'avez jamais fait, pour étudier le fonctionnement d'une merge request. Ce n'est pas le coeur du sujet mais c'est intéressant.

Observez ce qu'il se passe lorsqu'on crée la merge request. Si vous voyez que la pipeline est skipped, vous avez probablement fait une erreur. Laquelle ?

.. class::info
    
    Notez que les pipelines exécutées suite à une merge request sont taggées différemment
    .. image:: merge_request_pipeline.png

{{CORR

Correction
++++++++++++++

- Modifier le job generate_doc pour qu'il y ait une règle associée:

.. code-block:: yaml

    generate_doc:
      stage: doc
      script: tox -e docs
      rules:
        - if: $CI_PIPELINE_SOURCE == "merge_request_event"

- Constater que la pipeline qui se déclenche n'exécute plus la génération de doc
- Crée une branche (``git switch -c rule_doc``)
- Pusher la branche (``git push --set-upstream origin rule_doc``)
- Depuis la page principale, créer un merge request
- Voir le champ pipeline affiché après création


CORR}}

Packager automatiquement
---------------------------------------

Dans la phase de release, on veut généralement créer un package binaire. Ici en python, il existe une commande pour packager. Quelle-est-elle (Pensez à la doc de ``tox``) ?

Une fois le binaire créé, va-t-il rester accessible ? Pourquoi ?

Pour publier un binaire directement dans Gitlab, il existe la notion `d'artefact <https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html>`_.

En vous aidant de la documentation, générez un artefact accessible depuis l'interface Gitlab, lors du stage 'release', uniquement quand un nouveau tag est créé.

.. image:: artifacts.png

.. class::info

  vous aurez besoin, pour déclencher la création de l'artefact, de créer un tag sur votre dépôt git. n'oubliez pas de le pousser avec la commande ``git push --tags``

Connaissez vous d'autres opérations qui permettraient de packager automatiquement ? Écrivez une ligne de commande qui packagerai, et qui pourrait être ajouté à l'étape de release (par exemple en copiant le packet sur un ftp).

{{CORR

Correction
++++++++++++++

- Une fois le binaire créé, il ne reste pas accessible car une fois la pipeline terminée, le container est supprimé
- L'opération de packaging finale ressemblera à ceci:

.. code-block:: yaml

    build_package:
      stage: release
      script: tox -e build
      rules:
        - if: $CI_COMMIT_TAG
      artifacts:
        paths:
          - dist/*

Cf point suivant pour livrer / packager autre chose

CORR}}

Bonus
+++++++++

En utilisant la notion d'artefact et `cette doc <https://docs.gitlab.com/16.10/ee/ci/testing/unit_test_reports.html>`_, afficher les rapports de tests unitaires.

Livrer automatiquement
---------------------------------------

On se propose d'ajouter une étape qui pousse le packet que vous venez de créer vers un serveur ftp.
Modifiez votre fichier pour cela.

Je vous met à dispo le serveur suivant :

- ftp://formation-ci.radiko.fr
- login: ducobu
- password: ducobu
- port: 21

**Par défaut ftp est installé sur ubuntu (tnftp)**

Le logiciel lftp peut vous aider : https://doc.ubuntu-fr.org/lftp

Vérifier que votre packet est bien disponible en allant sur formation-ci.radiko.fr/ducobu/votredossier.

**Screenshot de votre paquet visible sur le ftp**

Voyez vous un problème avec la méthode que vous avez utilisé ? Qu'est ce que Gitlab préconise ? Vous pouvez vous inspirer de https://docs.gitlab.com/ee/ci/environments/ pour répondre.

Plutôt qu'un FTP, comment pouvez vous livrer votre release ?
(Vous pouvez vous inspirer de https://docs.gitlab.com/ee/user/project/releases/release_cicd_examples.html, mais aussi package repositories).

{{CORR

.. code-block:: bash

    lftp ftp://formation-python.radiko.fr -u ducobu,ducobu -p 21 -e "mirror -e -R docs/_build/html/ /groupe1 ; quit"

CORR}}

Publier la documentation automatiquement
------------------------------------------------------

Jetez un œil à: https://www.mkdocs.org/user-guide/deploying-your-docs/, qui propose un certain nombre de manières de déployer une documentation générée automatiquement. Il manque notamment dans leur liste Gitlab pages qui permet de le faire simplement également.

.. class::info
  - MkDoc est un générateur de pages, donc ils proposent leur outil dans leurs exemple, mais ils sont applicables à n'importe quel outil (notamment sphinx)
  - Gitlab Pages n'est pas activé sur notre serveur de test.

Effectuez un déploiement de votre documentation sur formation-python.radiko.fr:2015/ducobu/<votreprojet> (cf paragraphe Livrer Automatiquement).

Liens divers
--------------

- Architecture de gitlab : https://docs.gitlab.com/ee/development/architecture.html
- Monitorer gitlab : https://docs.gitlab.com/runner/monitoring/
- RedHat à propos de la CI/CD :  https://www.redhat.com/en/topics/devops/what-is-ci-cd ou en francais : https://www.redhat.com/fr/topics/devops/what-is-ci-cd
