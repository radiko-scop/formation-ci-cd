.. sectnum::

TP1 : Intégration continue partie 2
====================================

Rappel de la partie précédente
--------------------------------

Dans la partie précédente on a vu:

- Qu'on pouvait exécuter automatiquement des tâches lorsqu'on modifie la base de code
- Qu'il n'y avait pas de limite à ce qu'on pouvait exécuter lors de cette phase:
    - L'exécution se fait dans un container, on peut utiliser tout les outils que l'on veut dans ce container
    - L'exécution peut être conditionnelle
    - Typiquement on voudra exécuter des tests


Analyse de code & métriques
-------------------------------

Souvent, on paire l'intégration continue à un outil externe, qui va effectuer des analyse plus poussées sur le code, et surtout présenter les résultats de manière intelligible.

Nous allons étudier l'exemple SonarQube (SonarCloud pour sa version SAAS) : https://www.sonarqube.org/

**Attention: la version community de sonarqube ne gère qu'un repo / groupe pour github et gitlab -> tous les élèves ne pourront pas le faire -> il faudrait trouver mieux. Une option c'est qu'ils appartiennen ttous à un même groupe j'imagine. **

- Connectez vous à http://51.91.148.161:9000/
- identifiants : admin / @i+]W"t+e7._UbX (c'est une instance de démo)
- Cliquez sur create project
- Sélectionnez votre projet dans la liste et suivez les instructions

Observez les différentes métriques proposées. Qu'est ce qui vous parait le plus intéressant ?

Assurez vous d'avoir fait fonctionner le coverage (https://docs.sonarqube.org/latest/analysis/test-coverage/python-test-coverage/).

<Screenshot de votre coverage>

Certaines analyses peuvent être exécutées en local avant le commit : SonarLint
-----------------------------------------------------------------------------------------------------

On ne va pas regarder dans le détail, mais sachez que le résultat de l'analyse de SonarQube peut être directement intégré dans votre IDE : https://docs.sonarqube.org/latest/user-guide/connected-mode/

Utilisation du cache
---------------------------------------

Dans certains cas, les pipelines sont trop lentes. La cause peut être le téléchargement d'un nombre important de packets tiers. Pour éviter de télécharger à nouveau à chaque fois les paquets, Gitlab propose de garder un cache en local. Cela ne concerne pas les images, qui sont à priori cachées automatiquement par docker.

Proposez, en vous basant sur https://docs.gitlab.com/ee/ci/caching/, un bout de fichier .gitlab-ci.yml permettant de mettre en cache les paquets dont vous avez besoin.

Améliorer et monitorer ses pipelines / runners
------------------------------------------------

On ne va pas se lancer là dedans dans ce TP, mais il peut être intéressant de lire : https://docs.gitlab.com/ee/ci/pipelines/pipeline_efficiency.html

Certaines parties peuvent être exécutées en local avant le commit : les pre-commit hooks
-----------------------------------------------------------------------------------------------------

Certaines opérations peuvent être exécutées localement. Git fournit la possibilité, grâce à des hooks (https://git-scm.com/docs/githooks), d'exécuter des scripts sur évènement. Typiquement de vérifier que les rêgles de codage sont respectées, avant de pusher. Un outils puissant permet de partager et d'avoir des scripts tout faits pour différent langages : https://pre-commit.com/.

En vous aidant de la page https://verdantfox.com/blog/view/how-to-use-git-pre-commit-hooks-the-hard-way-and-the-easy-way, crééz un pre-commit hook qui analyse la syntaxe de votre code avant envoi.

.. class::info

  pyscaffold propose de générer un pre-commit hook, regarder la documentation.

<Ajoutez un screenshot de votre hook fonctionnant à votre compte rendu>.

Debugging pipelines
-----------------------------------

On ne va pas faire cette partie. Sachez simplement que si besoin, avec un runner bien configuré, on peut avoir accès à un terminal pour le débugger : https://docs.gitlab.com/ee/ci/interactive_web_terminal/

Comparaison avec la configuration proposée par pyscaffold
----------------------------------------------------------

- Assurez vous d'avoir un dépot propre
- Lancez : ``putup --update --gitlab formation-ci-cd/`` dans un terminal.
- Un fichier .gitlab-ci.yaml est créé. Expliquez le contenu du fichier obtenu.

.. class::info

  Pyscaffold propose des configurations de bases pour d'autres outils de CI, notamment Github. Voir la doc pour plus d'infos.

Bonne pratiques
---------------------

Lisez la page : https://about.gitlab.com/blog/2022/02/03/how-to-keep-up-with-ci-cd-best-practices/.
Dans quelle mesure respectez vous ces pratiques dans ce TP ? Que devrait-on améliorer ?

Autres informations utiles
----------------------------------------------------------------------

Ajouter des supers macarons
+++++++++++++++++++++++++++++++

- Gitlab : https://docs.gitlab.com/ee/user/project/badges.html
- Github : https://docs.github.com/en/actions/monitoring-and-troubleshooting-workflows/adding-a-workflow-status-badge


Analytics
++++++++++++

Voir les pages gitlab liées:

.. image:: analytics.png

Limites de la CI
-----------------

Voyez vous des limites à l'utilisation des outils d'automatisation ?

Avez vous une idée de cas dans lesquelles un bon workflow couplé à une CI ne suffisent pas à faire une release ?


Des exemples plus complexes
------------------------------------

Étudiez une pipeline plus complexe, d'un projet (ImageMagick, GTK (https://gitlab.gnome.org/GNOME/gtk/-/blob/main/.gitlab-ci.yml), Linux, https://github.com/mozilla,  https://firefox-source-docs.mozilla.org/taskcluster/taskgraph.html, chromium, vscode, ... les exemples ne manquent pas). 

Pour trouver des idées, vous pouvez soit utiliser la fonction explore de github, soit réfléchir aux logiciels open source que vous utilisez et regarder comment ils sont déployés, soit réfléchir à une entreprise que vous appréciez et regarder si elle a un compte github / gitlab ou autre public sur lequel des projets sont déployés ...


Qu'en pensez vous ? Voyez vous des choses pertinentes ?

Je propose, si on a le temps de faire la restitution de cette partie à l'oral pour échanger avec les autres.


Bonus
-------------

Proposez un schéma de pipeline que vous pourriez mettre en place au travail (ou présentez le schéma existant, et les améliorations que vous pourriez proposer en prenant en compte ce cours).
On pourra jeter un oeil à https://docs.gitlab.com/ee/ci/pipelines/
